<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Origin, Authorization, -token');
header('Access-Control-Allow-Credentials: true');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->group(function () {

    Route::prefix('auth')->group(function(){        
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::get('me', 'AuthController@me');
    });

    Route::prefix('/animais')->group(function () {
        Route::get('/', 'AnimalController@index');
        Route::post('/search', 'AnimalController@search');
        Route::get('/find/{id}', 'AnimalController@find');
        //TODO: mover para auth
        Route::post('/save', 'AnimalController@save_update');
        Route::get('/get_all_by_user/{user_id}', 'AnimalController@get_all_by_user');
    });

    Route::prefix('/cidades')->group(function () {
        Route::get('/', 'CidadeController@index');
        Route::get('/find_by_state_id/{state_id}', 'CidadeController@find_by_state_id');
    });

    Route::prefix('/estados')->group(function () {
        Route::get('/', 'EstadoController@index');
    });

    Route::prefix('/eventos')->group(function () {
        Route::get('/', 'EventoController@index');
        Route::get('/find/{id}', 'EventoController@find');
    });

    Route::prefix('/ong')->group(function () {
        Route::get('/', 'OngController@index');
        Route::get('/find/{id}', 'OngController@find');
    });

    Route::prefix('/racas')->group(function () {
        Route::get('/', 'RacaController@index');
        Route::get('/find/{id}', 'RacaController@find');
        Route::get('/find_by_type/{type}', 'RacaController@find_by_type');
    });

    Route::prefix('/usuarios')->group(function () {
        Route::get('/find/{id}', 'UsuarioController@find');
        Route::post('/save', 'UsuarioController@save_update');
    });

    Route::middleware('auth:api')->group(function() {
        Route::get('/projetos', function () {
            return '123';
        });
    });

});
