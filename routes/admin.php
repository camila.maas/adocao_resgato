<?php
//Se não ta logado, entra nesses "métodos abaixo", senão redireciona pro middleware debaixo 
Route::prefix('login')->name('login')->middleware('admin.redirectIfAuthenticatedAdmin')->group(function () {
    Route::view('/', 'admin.login');
    Route::post('/', 'AuthController@login')->name('.post');
});

Route::get('/logout', 'AuthController@logout')->name('logout');

Route::middleware('admin.authenticate')->group(function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::prefix('/animais')->name('animais.')->group(function () {
        Route::get('/', 'AnimalController@index')->name('index');
        Route::get('/find/{id}', 'AnimalController@find');
        Route::get('/create', 'AnimalController@create_save_view')->name('create_save_view');
        Route::get('/edit/{id}', 'AnimalController@create_edit_view')->name('create_edit_view');

        Route::post('/save', 'AnimalController@save_update')->name('save_update');
        Route::get('/delete/{id}', 'AnimalController@delete')->name('delete');
        Route::post('/delete_image/', 'AnimalController@delete_image')->name('delete_image');
        Route::post('/alterar_status', 'AnimalController@alterar_status')->name('alterar_status');
        Route::post('/abrir_modal_status', 'AnimalController@abrir_modal_status')->name('abrir_modal_status');
    });

    Route::prefix('/eventos')->name('eventos.')->group(function () {
        Route::get('/', 'EventoController@index')->name('index');
        Route::get('/find/{id}', 'EventoController@find');
        Route::get('/create', 'EventoController@create_save_view')->name('create_save_view');
        Route::get('/edit/{id}', 'EventoController@create_edit_view')->name('create_edit_view');
        Route::post('/delete_image/', 'EventoController@delete_image')->name('delete_image');

        Route::post('/save', 'EventoController@save_update')->name('save_update');
        Route::get('/delete/{id}', 'EventoController@delete')->name('delete');
    });

    Route::prefix('/ong')->name('ong.')->group(function () {
        Route::get('/', 'OngController@index')->name('index');
        Route::get('/find/{id}', 'OngController@find');        
        Route::get('/create', 'OngController@create_save_view')->name('create_save_view');
        Route::get('/edit/{id}', 'OngController@create_edit_view')->name('create_edit_view');
        
        Route::post('/save', 'OngController@save_update')->name('save_update');
        Route::get('/delete/{id}', 'OngController@delete')->name('delete');
        Route::post('/delete_image/', 'OngController@delete_image')->name('delete_image');
    });

    Route::prefix('/racas')->name('racas.')->group(function () {
        Route::get('/', 'RacaController@index')->name('index');
        Route::get('/find/{id}', 'RacaController@find');
        Route::get('/create', 'RacaController@create_save_view')->name('create_save_view');
        Route::get('/edit/{id}', 'RacaController@create_edit_view')->name('create_edit_view');
        
        Route::post('/save', 'RacaController@save_update')->name('save_update');
        Route::get('/delete{id}', 'RacaController@delete')->name('delete');
        Route::post('/find_by_type', 'RacaController@find_by_type')->name('find_by_type');
    });

    Route::prefix('/usuarios')->name('usuarios.')->group(function () {
        Route::get('/', 'UsuarioController@index')->name('index');
        Route::get('/find/{id}', 'UsuarioController@find');
        Route::get('/create', 'UsuarioController@create_save_view')->name('create_save_view');
        Route::get('/edit/{id}', 'UsuarioController@create_edit_view')->name('create_edit_view');
        
        Route::post('/save', 'UsuarioController@save_update')->name('save_update');
        Route::get('/delete{id}', 'UsuarioController@delete')->name('delete');
    });

    Route::prefix('/admins')->name('admins.')->group(function () {
        Route::get('/', 'AdminController@index')->name('index');
        Route::get('/find/{id}', 'AdminController@find');
        Route::get('/create', 'AdminController@create_save_view')->name('create_save_view');
        Route::get('/edit/{id}', 'AdminController@create_edit_view')->name('create_edit_view');
        
        Route::post('/save', 'AdminController@save_update')->name('save_update');
        Route::get('/delete{id}', 'AdminController@delete')->name('delete');
    });


    Route::prefix('/cidades')->name('cidades.')->group(function () {
        Route::get('/', 'CidadeController@index')->name('index');
        Route::post('/find_by_state_id', 'CidadeController@find_by_state_id')->name('find_by_state_id');
    });


    Route::middleware('auth:api')->group(function() {
        Route::get('/projetos', function () {
            return '123';
        });
    });

});
