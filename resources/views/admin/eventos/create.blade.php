@extends('admin.eventos.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.eventos.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome">
                </div>
                {{-- <div class="container">
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker6'>
                                    <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker7'>
                                    <input type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                <div class="form-group">
                    <label for="start">Início</label>
                    <input type="datetime" class="form-control" name="start" placeholder="Início">
                </div>
                <div class="form-group">
                    <label for="end">Fim</label>
                    <input type="datetime" class="form-control" name="end" placeholder="Fim">
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3"> </textarea>
                </div>
                <div class="form-group">
                    <label for="ong">Ong</label>
                    <select class="form-control" name="ong">
                        <option value="">Selecione</option>
                        @foreach($ong as $o)
                        <option value="{{$o->id}}">{{$o->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="place">Local</label>
                    <input type="text" class="form-control" name="place" placeholder="Local">
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)">
                        <option value="">Selecione</option>
                        @foreach($estados as $estado)
                        <option value="{{$estado->id}}">{{$estado->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="city_id">
                    <label for="city_id">Cidade</label>
                    <select class="form-control" name="city_id">
                        <option value="">Selecione o estado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP">
                </div>
                <div class="form-group">
                    <label for="street">Rua</label>
                    <input type="text" class="form-control" name="street" placeholder="Rua">
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" placeholder="Bairro">
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento">
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" placeholder="Número">
                </div>
                <div id="images">
                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>
                </div>
                <button type="button" class="btn btn-info btn-xs" onclick="add_images()">Mais fotos</button>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.cidades.find_by_state_id')}}",
            data: 'id=' + state_id,
            dataType: 'JSON',
            success: function(msg) {
                $('#city_id').html(msg.view);
            },
            error: function(msg) {

            }
        })
    }


    function add_images() {
        $('#images')
            .append(`<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>`);
    }

    function remove_image(element) {
        $(element).parent().parent().remove();
    }

    // $(function () {
    //     $('#datetimepicker6').datetimepicker();
    //     $('#datetimepicker7').datetimepicker({
    //         useCurrent: false //Important! See issue #1075
    //     });
    //     $("#datetimepicker6").on("dp.change", function (e) {
    //         $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    //     });
    //     $("#datetimepicker7").on("dp.change", function (e) {
    //         $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    //     });
    // });
</script>
@endsection