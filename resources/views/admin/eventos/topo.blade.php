@extends('admin.layout')

@section('title', 'Eventos')

@section('content')

<div class="col-xs-12">
    <div class="conteudo">
        <h3 class="titulo_geral">
            <i class="fa fa-calendar" aria-hidden="true"></i> Eventos
        </h3>
        <a href="{{route('admin.eventos.create_save_view')}}" type="button" class="btn btn-default pull-right">
            Cadastrar
        </a>
    </div>
</div>

@if(session('message'))

    <div class="col-xs-12">
        <div class="alert alert-{{session('message.type')}}">
            {{session('message.message')}}
        </div>
    </div>

@endif

@include('admin.components.errors')

@yield('modulo')

@endsection