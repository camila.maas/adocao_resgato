@extends('admin.eventos.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.eventos.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf
                <input type="hidden" name="id" value="{{$evento->id}}">
                <input type="hidden" name="address_id" value="{{$evento->id_endereco}}">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{$evento->nome}}">
                </div>
                <div class="form-group">
                    <label for="start">Início</label>
                    <input type="text" class="form-control" name="start" placeholder="Início" value="@datetime($evento->data_hora_inicio)">
                </div>
                <div class="form-group">
                    <label for="end">Fim</label>
                    <input type="text" class="form-control" name="end" placeholder="Fim" value="@datetime($evento->data_hora_fim)">
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3">{{$evento->descricao}}</textarea>
                </div>
                <div class="form-group">
                    <label for="ong">Ong</label>
                    <select class="form-control" name="ong">
                        <option value="">Selecione</option>
                        @foreach($ong as $o)
                        <option @if($evento->id_ong == $o->id) selected @endif value="{{$evento->id_ong}}">{{$o->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="place">Local</label>
                    <input type="text" class="form-control" name="place" placeholder="Local" value="{{$evento->local}}">
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)">
                        <option value="">Selecione</option>
                        @foreach($estados as $estado)
                        <option @if($evento->getEndereco->getCidade->getEstado->id == $estado->id) selected @endif value="{{$estado->id}}">{{$estado->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="city_id">
                    <label for="city_id">Cidade</label>
                    <select class="form-control" name="city_id">
                        <option value="">Selecione o estado</option>
                        @foreach($cidades as $cidade)
                        <option @if($evento->getEndereco->id_cidade == $cidade->id) selected @endif value="{{$cidade->id}}">{{$cidade->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP" value="{{$evento->getEndereco->cep}}">
                </div>
                <div class="form-group">
                    <label for="street">Rua</label>
                    <input type="text" class="form-control" name="street" placeholder="Rua" value="{{$evento->getEndereco->rua}}">
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" placeholder="Bairro" value="{{$evento->getEndereco->bairro}}">
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento" value="{{$evento->getEndereco->complemento}}">
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" placeholder="Número" value="{{$evento->getEndereco->numero}}">
                </div>
                <div class="form-group" id="evento_images">
                    @if(count($evento->getFotos) > 0)
                    <label for="photo">Foto</label>
                    <div class="row">
                        @foreach($evento->getFotos as $foto)
                        <div class="col-md-4">
                            <img src="{{asset('storage/'. $foto->caminho)}}" class="img-responsive" alt="">
                            <center>
                                <button type="button" class="btn btn-danger btn-xs" onclick="delete_image(this, {{$foto->id}})">Remover</button>
                            </center>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                <center>
                    <button type="button" class="btn btn-info btn-xs" onclick="add_images()">Mais fotos</button>
                </center>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.cidades.find_by_state_id')}}",
            data: 'id=' + state_id,
            dataType: 'JSON',
            success: function(msg) {
                $('#city_id').html(msg.view);
            },
            error: function(msg) {

            }
        })
    }
    function add_images() {
        $('#evento_images')
            .append(`<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>`);
    }

    function delete_image(element, id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.eventos.delete_image')}}",
            data: 'id=' + id,
            dataType: 'JSON',
            success: function(msg) {
                remove_image(element);
            },
            error: function(msg) {
            }
        })
    }

    function remove_image(element){
        $(element).parent().parent().remove();
    }

</script>
@endsection