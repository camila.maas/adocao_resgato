@extends('admin.eventos.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Início</th>
                    <th>Fim</th>
                    <th>Local</th>
                    <th>Cidade-UF</th>
                    <th></th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($eventos as $e)

                    <tr>
                        <td>{{$e->nome}}</td>
                        <td>{{$e->descricao}}</td>
                        <td>@datetime($e->data_hora_inicio)</td>
                        <td>@datetime($e->data_hora_fim)</td>
                        <td>{{$e->local}}</td>
                        <td>{{$e->getEndereco->getCidade->nome}}-{{$e->getEndereco->getCidade->uf}}</td>
                        <td></td>
                        <td>
                            <a href="{{route('admin.eventos.create_edit_view', $e->id)}}" class="btn btn-xs btn-warning">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="{{route('admin.eventos.delete', $e->id)}}" class="btn btn-xs btn-danger" data-toggle="confirmation"
                                data-btn-ok-label="Sim" data-btn-ok-class="btn-sm btn-success"
                                data-btn-ok-icon="fa fa-check"
                                data-btn-cancel-label="Não" data-btn-cancel-class="btn-sm btn-danger"
                                data-btn-cancel-icon="fa fa-trash"
                                data-title="Excluir?">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection