@extends('admin.admins.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.admins.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <input type="hidden" name="id" value="{{$admin->id}}">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{$admin->nome}}">
                </div>
                <div class="form-group" id="level">
                    <label for="level">Nível</label>
                    <select class="form-control" name="level">
                        <option value="">Selecione o nível</option>
                        <option @if($admin->nivel == 1) selected @endif value="1">Adminsitrador Sistema</option>
                        <option @if($admin->nivel == 2) selected @endif value="2">Administrador Ong</option>
                    </select>
                </div>
                <div class="form-group" id="ong">
                    <label for="ong">Ong</label>
                    <select class="form-control selectpicker" name="ong[]" multiple>
                        <option value="">Selecione</option>
                        @foreach($ong as $o)
                            <option @if($admin->getOng()->get()->contains($o->id)) selected @endif value="{{$o->id}}">{{$o->nome}}</option>
                        @endforeach
                    </select>
                </div>   
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{$admin->email}}">
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id){
        $.ajax({
                method: 'POST',
                url: "{{route('admin.cidades.find_by_state_id')}}",
                data:'id='+state_id,
                dataType: 'JSON',
                success: function(msg){
                    $('#city_id').html(msg.view);
                }, error: function(msg){

                }
            })
    }
</script>
@endsection