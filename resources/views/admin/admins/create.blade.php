@extends('admin.admins.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.admins.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome">
                </div>
                <div class="form-group" id="level">
                    <label for="level">Nível</label>
                    <select class="form-control" name="level" onchange="enable_ong(this.value)">
                        <option value="">Selecione o nível</option>
                        <option value="1">Administrador Sistema</option>
                        <option value="2">Adinistrador Ong</option>
                    </select>
                </div>  
                <div class="form-group" id="ong">
                    <label for="ong">Ong</label>
                    <select class="form-control selectpicker" name="ong[]" multiple>
                        @foreach($ong as $o)
                            <option value="{{$o->id}}">{{$o->nome}}</option>
                        @endforeach
                    </select>
                </div>   
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail">
                </div>               
                <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" class="form-control" name="password" placeholder="Senha">
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function enable_ong(level){
        console.info(level);
        if(level != 2){
            $('#ong').hide();
        }else{
            $('#ong').show();
        }
    }
</script>
@endsection