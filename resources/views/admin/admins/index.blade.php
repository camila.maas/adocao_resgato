@extends('admin.admins.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Nível</th>
                    <th>E-mail</th>
                    <th></th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($admins as $a)

                    <tr>
                        <td>{{$a->nome}}</td>
                        <td>@nivel_admin($a->nivel)</td>
                        <td>{{$a->email}}</td>
                        {{-- <td>{{$a->getOng->first->nome}}</td> --}}
                        <td></td>
                        <td>
                            @if($a->id != 1)
                                <a href="{{route('admin.admins.create_edit_view', $a->id)}}" class="btn btn-xs btn-warning">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="{{route('admin.admins.delete', $a->id)}}" class="btn btn-xs btn-danger" data-toggle="confirmation"
                                    data-btn-ok-label="Sim" data-btn-ok-class="btn-sm btn-success"
                                    data-btn-ok-icon="fa fa-check"
                                    data-btn-cancel-label="Não" data-btn-cancel-class="btn-sm btn-danger"
                                    data-btn-cancel-icon="fa fa-trash"
                                    data-title="Excluir?">
                                    <i class="fas fa-trash"></i>
                                </a>
                            @endif
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection