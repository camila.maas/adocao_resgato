@extends('admin.ong.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.ong.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome">
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="text" class="form-control" name="phone" placeholder="Telefone">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <label for="responsible">Responsável</label>
                    <input type="text" class="form-control" name="responsible" placeholder="Responsável">
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)">
                        <option value="">Selecione</option>
                        @foreach($estados as $estado)
                            <option value="{{$estado->id}}">{{$estado->nome}}</option>
                        @endforeach
                    </select>
                </div>      
                <div class="form-group" id="city_id">
                        <label for="city_id">Cidade</label>
                        <select class="form-control" name="city_id">
                            <option value="">Selecione o estado</option>
                        </select>
                </div>               
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP">
                </div>
                <div class="form-group">
                        <label for="street">Rua</label>
                        <input type="text" class="form-control" name="street" placeholder="Rua">
                </div>
                <div class="form-group">
                        <label for="district">Bairro</label>
                        <input type="text" class="form-control" name="district" placeholder="Bairro">
                </div>  
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento">
            </div>  
            <div class="form-group">
                <label for="number">Número</label>
                <input type="text" class="form-control" name="number" placeholder="Número">
            </div>
                <div class="form-group" id="admins">
                    <label for="admins">Administradores</label>
                    <select class="form-control selectpicker" name="admins[]" multiple>
                        @foreach($admins as $a)
                            <option value="{{$a->id}}">{{$a->nome}}</option>
                        @endforeach
                    </select>
                </div>   
                <div class="form-group">
                    <label for="logo">Logo</label>
                    <input type="file" name="file">
                    <p class="help-block">Adicione uma logo para a Ong.</p>
                </div>
                <div id="images">
                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>
                </div>
                <button type="button" class="btn btn-info btn-xs" onclick="add_images()">Mais fotos</button>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id){
        $.ajax({
                method: 'POST',
                url: "{{route('admin.cidades.find_by_state_id')}}",
                data:'id='+state_id,
                dataType: 'JSON',
                success: function(msg){
                    $('#city_id').html(msg.view);
                }, error: function(msg){

                }
            })
    }

    function add_images(){
        $('#images')
        .append(`<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>`);
    }

    function remove_image(element){
        $(element).parent().parent().remove();
    }

</script>
@endsection