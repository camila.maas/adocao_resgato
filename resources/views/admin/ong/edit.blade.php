@extends('admin.ong.topo') @section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.ong.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}} @csrf
                <input type="hidden" name="id" value="{{$ong->id}}">
                <input type="hidden" name="address_id" value="{{$ong->id_endereco}}">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{$ong->nome}}">
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="text" class="form-control" name="phone" placeholder="Telefone" value="{{$ong->telefone}}">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{$ong->email}}">
                </div>
                <div class="form-group">
                    <label for="responsible">Responsável</label>
                    <input type="text" class="form-control" name="responsible" placeholder="Responsável" value="{{$ong->responsavel}}">
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)">
                        <option value="">Selecione</option>
                        @foreach($estados as $estado)
                        <option @if($ong->getEndereco->getCidade->getEstado->id == $estado->id) selected @endif value="{{$estado->id}}">{{$estado->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="city_id">
                    <label for="city_id">Cidade</label>
                    <select class="form-control" name="city_id">
                        <option value="">Selecione a cidade</option>
                        @foreach($cidades as $cidade)
                        <option @if($ong->getEndereco->id_cidade == $cidade->id) selected @endif value="{{$cidade->id}}">{{$cidade->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP" value="{{$ong->getEndereco->cep}}">
                </div>
                <div class="form-group">
                    <label for="street">Rua</label>
                    <input type="text" class="form-control" name="street" placeholder="Rua" value="{{$ong->getEndereco->rua}}">
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" placeholder="Bairro" value="{{$ong->getEndereco->bairro}}">
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento" value="{{$ong->getEndereco->complemento}}">
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" placeholder="Número" value="{{$ong->getEndereco->numero}}">
                </div>
                <div class="form-group" id="admins">
                    <label for="admins">Administradores</label>
                    <select class="form-control selectpicker" name="admins[]" multiple>
                        @foreach($admins as $a)
                            <option @if($ong->getAdmins()->get()->contains($a->id)) selected @endif value="{{$a->id}}">{{$a->nome}}</option>
                        @endforeach
                    </select>
                </div>   
                <div class="form-group">
                    <label for="logo">Logo</label>
                    <input type="file" name="file">
                    <img src="{{asset('storage/'. $ong->logo)}}" class="img-responsive" alt="">
                </div>
                <div class="form-group" id="ong_images">
                    @if(count($ong->getFotos) > 0)
                        <label for="photo">Foto</label>
                        <div class="row">
                            @foreach($ong->getFotos as $foto)
                            <div class="col-md-4">
                                <img src="{{asset('storage/'. $foto->caminho)}}" class="img-responsive" alt="">
                                <center>
                                    <button type="button" class="btn btn-danger btn-xs" onclick="delete_image(this, {{$foto->id}})">Remover</button>
                                </center>
                            </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                <center>
                    <button type="button" class="btn btn-info btn-xs" onclick="add_images()">Mais fotos</button>
                </center>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection @section('scripts')
<script>
    function change_cities(state_id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.cidades.find_by_state_id')}}",
            data: 'id=' + state_id,
            dataType: 'JSON',
            success: function(msg) {
                $('#city_id').html(msg.view);
            },
            error: function(msg) {

            }
        })
    }

    function add_images() {
        $('#ong_images')
            .append(`<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>`);
    }

    function delete_image(element, id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.ong.delete_image')}}",
            data: 'id=' + id,
            dataType: 'JSON',
            success: function(msg) {
                remove_image(element);
            },
            error: function(msg) {
            }
        })
    }

    function remove_image(element){
        $(element).parent().parent().remove();
    }

</script>
@endsection