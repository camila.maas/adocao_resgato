@extends('admin.ong.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>E-mail</th>
                    <th>Responsável</th>
                    <th>Cidade-UF</th>
                    <th></th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ong as $o)

                    <tr>
                        <td>{{$o->nome}}</td>
                        <td>{{$o->telefone}}</td>
                        <td>{{$o->email}}</td>
                        <td>{{$o->responsavel}}</td>
                        <td>{{$o->getEndereco->getCidade->nome}}-{{$o->getEndereco->getCidade->uf}}</td>
                        <td></td>
                        <td>
                            <a href="{{route('admin.ong.create_edit_view', $o->id)}}" class="btn btn-xs btn-warning">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="{{route('admin.ong.delete', $o->id)}}" class="btn btn-xs btn-danger" data-toggle="confirmation"
                                data-btn-ok-label="Sim" data-btn-ok-class="btn-sm btn-success"
                                data-btn-ok-icon="fa fa-check"
                                data-btn-cancel-label="Não" data-btn-cancel-class="btn-sm btn-danger"
                                data-btn-cancel-icon="fa fa-trash"
                                data-title="Excluir?">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection