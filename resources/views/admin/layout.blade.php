<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>::. @yield('title') .::</title>
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <!-- Styles -->
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/style.css') }}" rel="stylesheet">
    <!----------------------------- FONTAWESOME ----------------------------->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    @yield('styles')
    
</head>

<body>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        
        <div id="logo">
            <center>
                <!-- LOGO -->
               <a href="">
                    <img src="{{ asset('images/logo_branca.png') }}" class="img-responsive" width="150">
                </a> 
            </center>
        </div>

        <ul class="sidebar-nav">
            <li class="active">
                <a href="{{route('admin.dashboard')}}"><i class="fas fa-tachometer-alt" aria-hidden="true"></i> Inicio</a>
            </li>           
            <li>
                <a href="{{route('admin.ong.index')}}"><i class="fa fa-users" aria-hidden="true"></i> Ong</a>
            </li>  
            <li>
                <a href="{{route('admin.animais.index')}}"><i class="fa fa-paw" aria-hidden="true"></i> Animais</a>
            </li>           
            <li>
                <a href="{{route('admin.racas.index')}}"><i class="fa fa-tag" aria-hidden="true"></i> Raças</a>
            </li>           
            <li>
                <a href="{{route('admin.eventos.index')}}"><i class="fa fa-calendar" aria-hidden="true"></i> Eventos</a>
            </li>           
            <li>
                <a href="{{route('admin.usuarios.index')}}"><i class="fa fa-address-card" aria-hidden="true"></i> Usuários</a>
            </li>  
            <li>
                <a href="{{route('admin.admins.index')}}"><i class="fa fa-address-card" aria-hidden="true"></i> Administradores</a>
            </li>           
        </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">

        <!------ MENU ------>

        <nav class="navbar navbar-primary navbar-fixed">
            <div class="container-fluid">

                <div class="navbar-header" style="width: 100%;">
                    <button type="button" id="sidebarCollapse" class="btn btn-default navbar-btn">
                        <i class="fas fa-bars"></i>
                    </button>

                    <a href="{{route('admin.logout')}}" type="button" id="sidebarCollapse" class="sair_admin btn btn-default navbar-btn pull-right" style="color: #0c3e46;background: none;">
                        Sair
                    </a>
                </div>
            </div>
        </nav>

        <!------ CONTEUDO ------>

        <div id="conteudo_pagina">

            @yield('content')

        </div>

    </div>
</div>

<script src="{{asset('js/admin/app.js')}}"></script>

@yield('scripts')

<script>
//Mandar o csrf token para rota laravel
$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    })

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
    })

    $('.datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        language: 'pt-BR'
    })

    $('.mask-cpf').mask('000.000.000-00')
    $('.mask-cnpj').mask('00.000.000/0000-00')

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009'
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options)
        }
    }

    $('.selectpicker').selectpicker();

    $('.mask-telefone').mask(SPMaskBehavior, spOptions)

    $('.summernote').summernote()
})
</script>

</body>
</html>