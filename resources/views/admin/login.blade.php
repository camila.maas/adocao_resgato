
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>::. Admin .::</title>
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin/style.css') }}" rel="stylesheet">

</head>
<body>
    <div id="login_container">
        <div class="container">
            <div class="card card-container">
                <center>
                   <img class="img-responsive" src="{{asset('images/logo.png')}}" width="250" />
                </center>
                <p id="profile-name" class="profile-name-card"></p>
                <form class="form-signin" action="{{route('admin.login.post')}}" method="POST">
                {{csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Senha" name="password" required value="{{old('password')}}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-block g-recaptcha" type="submit" data-sitekey="{{env('RECAPTCHA_PUBLIC_KEY')}}" data-callback="login">Login</button>
                    </div>
                </form><!-- /form -->
                @if ($errors->any())
                    <div class="alert alert-danger text-danger">
                        <ul class="unstyle">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div><!-- /card-container -->
        </div><!-- /container -->
    </div>
</body>
<script src="{{ asset('js/admin/app.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    function login(token){
        $('form').submit()
    }
</script>
</html>