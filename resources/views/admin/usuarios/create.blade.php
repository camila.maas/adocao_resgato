@extends('admin.usuarios.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.usuarios.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome">
                </div>
                <div class="form-group">
                    <label for="birth_date">Data de nascimento</label>
                    <input type="text" class="form-control" name="birth_date" placeholder="Data de nascimento">
                </div>
                <div class="form-group" id="sex">
                    <label for="sex">Sexo</label>
                    <select class="form-control" name="sex">
                        <option value="">Selecione o sexo</option>
                        <option value="1">Masculino</option>
                        <option value="2">Feminino</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="text" class="form-control" name="phone" placeholder="Telefone">
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)">
                        <option value="">Selecione</option>
                        @foreach($estados as $estado)
                            <option value="{{$estado->id}}">{{$estado->nome}}</option>
                        @endforeach
                    </select>
                </div>      
                <div class="form-group" id="city_id">
                        <label for="city_id">Cidade</label>
                        <select class="form-control" name="city_id">
                            <option value="">Selecione o estado</option>
                        </select>
                </div>               
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP">
                </div>
                <div class="form-group">
                        <label for="street">Rua</label>
                        <input type="text" class="form-control" name="street" placeholder="Rua">
                </div>
                <div class="form-group">
                        <label for="district">Bairro</label>
                        <input type="text" class="form-control" name="district" placeholder="Bairro">
                </div>  
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento">
                </div>  
                <div class="form-group">
                        <label for="number">Número</label>
                        <input type="text" class="form-control" name="number" placeholder="Número">
                </div>
                
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail">
                </div>               
                <div class="form-group">
                    <label for="cep">Senha</label>
                    <input type="password" class="form-control" name="password" placeholder="Senha">
                </div>
                <div class="form-group">
                        <label for="file">Foto de perfil</label>
                        <input type="file" name="file">
                    </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id){
        $.ajax({
                method: 'POST',
                url: "{{route('admin.cidades.find_by_state_id')}}",
                data:'id='+state_id,
                dataType: 'JSON',
                success: function(msg){
                    $('#city_id').html(msg.view);
                }, error: function(msg){

                }
            })
    }
</script>
@endsection