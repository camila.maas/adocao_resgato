@extends('admin.usuarios.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.usuarios.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <input type="hidden" name="id" value="{{$usuario->id}}">
                <input type="hidden" name="address_id" value="{{$usuario->id_endereco}}">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{$usuario->nome}}">
                </div>
                <div class="form-group">
                    <label for="birth_date">Data de nascimento</label>
                    <input type="text" class="form-control" name="birth_date" placeholder="Data de nascimento" value="@date($usuario->data_nascimento)">
                </div>
                <div class="form-group" id="sex">
                    <label for="sex">Sexo</label>
                    <select class="form-control" name="sex">
                        <option value="">Selecione o sexo</option>
                        <option @if($usuario->sexo == 1) selected @endif value="1">Masculino</option>
                        <option @if($usuario->sexo == 2) selected @endif value="2">Feminino</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="phone">Telefone</label>
                    <input type="text" class="form-control" name="phone" placeholder="Telefone" value="{{$usuario->telefone}}">
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)">
                            <option value="">Selecione</option>
                            @foreach($estados as $estado)
                            <option @if($usuario->getEndereco->getCidade->getEstado->id == $estado->id) selected @endif value="{{$estado->id}}">{{$estado->nome}}</option>
                            @endforeach
                        </select>
                    </div>      
                    <div class="form-group" id="city_id">
                        <label for="city_id">Cidade</label>
                        <select class="form-control" name="city_id">
                            <option value="">Selecione o estado</option>
                            @foreach($cidades as $cidade)
                            <option @if($usuario->getEndereco->id_cidade == $cidade->id) selected @endif value="{{$cidade->id}}">{{$cidade->nome}}</option>
                            @endforeach
                        </select>
                    </div>               
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" name="cep" placeholder="CEP" value="{{$usuario->getEndereco->cep}}">
                    </div>
                    <div class="form-group">
                        <label for="street">Rua</label>
                        <input type="text" class="form-control" name="street" placeholder="Rua" value="{{$usuario->getEndereco->rua}}">
                    </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" placeholder="Bairro" value="{{$usuario->getEndereco->bairro}}">
                </div>  
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento" value="{{$usuario->getEndereco->complemento}}">
                </div>  
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" placeholder="Número" value="{{$usuario->getEndereco->numero}}">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{$usuario->email}}">
                </div>
                <div class="form-group">
                    <label for="file">Foto de perfil</label>
                    <input type="file" name="file">
                    <img src="{{asset('storage/'. $usuario->foto)}}" class="img-responsive" alt="">
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>

    function change_cities(state_id){
        $.ajax({
                method: 'POST',
                url: "{{route('admin.cidades.find_by_state_id')}}",
                data:'id='+state_id,
                dataType: 'JSON',
                success: function(msg){
                    $('#city_id').html(msg.view);
                }, error: function(msg){

                }
            })
    }

</script>
@endsection