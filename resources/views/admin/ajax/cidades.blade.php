<label for="city_id">Cidade</label>
<select class="form-control" name="city_id" required>
    @foreach($cidades as $cidade)
        <option value="{{$cidade->id}}">{{$cidade->nome}}</option>
    @endforeach
</select>