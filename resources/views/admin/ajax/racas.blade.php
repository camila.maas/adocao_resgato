<label for="breed_id">Raça</label>
<select class="form-control" name="breed_id" required>
    @foreach($racas as $raca)
        <option value="{{$raca->id}}">{{$raca->nome}}</option>
    @endforeach
</select>