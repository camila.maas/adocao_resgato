@extends('admin.animais.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.animais.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <input type="hidden" name="id" value="{{$animal->id}}">
                <input type="hidden" name="breed_id" value="{{$animal->id_raca}}">
                <input type="hidden" name="user_id" value="{{$animal->id_usuario}}">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{$animal->nome}}">
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3" >{{$animal->descricao}}</textarea>
                </div>
                <div class="form-group" id="animal_type">
                    <label for="animal_type">Tipo</label>
                    <select class="form-control" name="animal_type" onchange="change_types(this.value)" required>
                        <option value="">Selecione</option>
                        <option @if($animal->getRaca->tipo == 1) selected @endif value="1">Cachorro</option>
                        <option @if($animal->getRaca->tipo == 2) selected @endif value="2">Gato</option>
                    </select>
                </div>
                <div class="form-group" id="breed_id">
                    <label for="breed_id">Raça</label>
                    <select class="form-control" name="breed_id">
                        <option value="">Selecione o estado</option>
                        @foreach($racas as $raca)
                        <option @if($animal->id_raca == $raca->id) selected @endif value="{{$raca->id}}">{{$raca->nome}}</option>
                    @endforeach
                    </select>
                </div> 
                <div class="form-group">
                    <label for="color">Cor</label>
                    <input type="text" class="form-control" name="color" placeholder="Cor" value="{{$animal->cor}}">
                </div>
                <div class="form-group">
                    <label for="birth_date">Data de nascimento</label>
                    <input type="text" class="form-control" name="birth_date" placeholder="Data de nascimento" value="@date($animal->data_nascimento)">
                </div>
                
                <div class="form-group" id="size">
                    <label for="size">Porte</label>
                    <select class="form-control" name="size">
                        <option value="">Selecione o porte</option>
                        <option @if($animal->porte == 1) selected @endif value="1">Pequeno</option>
                        <option @if($animal->porte == 2) selected @endif value="2">Médio</option>
                        <option @if($animal->porte == 3) selected @endif value="3">Grande</option>
                    </select>
                </div>  
                <div class="form-group" id="sex">
                    <label for="sex">Sexo</label>
                    <select class="form-control" name="sex">
                        <option value="">Selecione o sexo</option>
                        <option @if($animal->sexo == 1) selected @endif value="1">Macho</option>
                        <option @if($animal->sexo == 2) selected @endif value="2">Fêmea</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="ong">Ong</label>
                    <select class="form-control" name="ong">
                        <option value="">Selecione</option>
                        @foreach($ong as $o)
                            <option @if($animal->id_ong == $o->id) selected @endif value="{{$o->id}}">{{$o->nome}}</option>
                        @endforeach
                    </select>
                    </div>      
                <div class="form-group">
                        <label for="state">Estado</label>
                        <select class="form-control" name="state" onchange="change_cities(this.value)">
                            <option value="">Selecione</option>
                            @foreach($estados as $estado)
                                <option @if($animal->getEndereco->getCidade->getEstado->id == $estado->id) selected @endif value="{{$estado->id}}">{{$estado->nome}}</option>
                            @endforeach
                        </select>
                    </div>      
                    <div class="form-group" id="city_id">
                            <label for="city_id">Cidade</label>
                            <select class="form-control" name="city_id">
                                <option value="">Selecione o estado</option>
                                @foreach($cidades as $cidade)
                                <option @if($animal->getEndereco->id_cidade == $cidade->id) selected @endif value="{{$cidade->id}}">{{$cidade->nome}}</option>
                            @endforeach
                            </select>
                    </div>               
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" class="form-control" name="cep" placeholder="CEP" value="{{$animal->getEndereco->cep}}">
                    </div>
                    <div class="form-group">
                            <label for="street">Rua</label>
                            <input type="text" class="form-control" name="street" placeholder="Rua" value="{{$animal->getEndereco->rua}}">
                    </div>
                    <div class="form-group">
                        <label for="district">Bairro</label>
                        <input type="text" class="form-control" name="district" placeholder="Bairro" value="{{$animal->getEndereco->bairro}}">
                    </div>  
                    <div class="form-group">
                            <label for="complement">Complemento</label>
                            <input type="text" class="form-control" name="complement" placeholder="Complemento" value="{{$animal->getEndereco->complemento}}">
                    </div>  
                    <div class="form-group">
                            <label for="number">Número</label>
                            <input type="text" class="form-control" name="number" placeholder="Número" value="{{$animal->getEndereco->numero}}">
                    </div>
                    <div class="form-group" id="ad_type">
                        <label for="ad_type">Tipo do anúncio</label>
                        <select class="form-control" name="ad_type" required>
                                <option value="">Selecione o tipo do anúncio</option>
                                <option @if($animal->tipo_anuncio == 1) selected @endif value="1">Adoção</option>
                                <option @if($animal->tipo_anuncio == 2) selected @endif value="2">Doação</option>
                                <option @if($animal->tipo_anuncio == 3) selected @endif value="3">Resgate</option>
                                <option @if($animal->tipo_anuncio == 4) selected @endif value="4">Desaparecido</option>
                                <option @if($animal->tipo_anuncio == 5) selected @endif value="5">Encontrado</option>
                        </select>
                    </div>
                    <div class="form-group" id="status">
                        <label for="status">Tipo do anúncio</label>
                        <select class="form-control" name="status" required>
                                <option value="">Selecione o tipo do anúncio</option>
                                <option @if($animal->status == 0) selected @endif value="0">Em análise</option>
                                <option @if($animal->tipo_anuncio == 1) selected @endif value="1">Aprovado</option>
                                <option @if($animal->tipo_anuncio == 2) selected @endif value="2">Reprovado</option>
                        </select>
                    </div>
                    <div class="form-group" id="images">
                        @if(count($animal->getFotos) > 0)
                            <label for="photo">Foto</label>
                            <div class="row">
                                @foreach($animal->getFotos as $foto)
                                <div class="col-md-4">
                                    <img src="{{asset('storage/'. $foto->caminho)}}" class="img-responsive" alt="">
                                    <center>
                                        <button type="button" class="btn btn-danger btn-xs" onclick="delete_image(this, {{$foto->id}})">Remover</button>
                                    </center>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                        <center>
                            <button type="button" class="btn btn-info btn-xs" onclick="add_images()">Mais fotos</button>
                        </center>
                    <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
function change_cities(state_id){
        $.ajax({
                method: 'POST',
                url: "{{route('admin.cidades.find_by_state_id')}}",
                data:'id='+state_id,
                dataType: 'JSON',
                success: function(msg){
                    $('#city_id').html(msg.view);
                }, error: function(msg){

                }
            })
    }
    
    function change_types(type){
        console.info('entrou');
        $.ajax({
                method: 'POST',
                url: "{{route('admin.racas.find_by_type')}}",
                data:'type='+type,
                dataType: 'JSON',
                success: function(msg){
                    $('#breed_id').html(msg.view);
                }, error: function(msg){
                }
            })
    }

    function add_images() {
        $('#images')
            .append(`<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>`);
    }

    function delete_image(element, id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.animais.delete_image')}}",
            data: 'id=' + id,
            dataType: 'JSON',
            success: function(msg) {
                remove_image(element);
            },
            error: function(msg) {
            }
        })
    }

    function remove_image(element){
        $(element).parent().parent().remove();
    }

</script>
@endsection