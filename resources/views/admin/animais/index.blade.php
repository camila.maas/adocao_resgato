@extends('admin.animais.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Cor</th>
                    <th>Porte</th>
                    <th>Sexo</th>
                    <th>Raça</th>
                    <th>Cidade-UF</th>
                    <th>Status</th>
                    <th></th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($animais as $a)

                <tr>
                    <td>{{$a->nome}}</td>
                    <td>{{$a->cor}}</td>
                    <td>@porte($a->porte)</td>
                    <td>@sexo_animal($a->sexo)</td>
                    <td>{{$a->getRaca->nome}}</td>
                    <td>{{$a->getEndereco->getCidade->nome}}-{{$a->getEndereco->getCidade->uf}}</td>
                    <td>@status($a->status)
                        <a href="javascript:void(0);" onclick="abrir_modal_status({{$a->id}});">
                            <i class="far fa-edit"></i>
                        </a>

                    </td>
                    <td></td>
                    <td>
                        <a href="{{route('admin.animais.create_edit_view', $a->id)}}" class="btn btn-xs btn-warning">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="{{route('admin.animais.delete', $a->id)}}" class="btn btn-xs btn-danger" data-toggle="confirmation" data-btn-ok-label="Sim" data-btn-ok-class="btn-sm btn-success" data-btn-ok-icon="fa fa-check" data-btn-cancel-label="Não" data-btn-cancel-class="btn-sm btn-danger" data-btn-cancel-icon="fa fa-trash" data-title="Excluir?">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar Status</h4>
            </div>
            <div class="modal-body">

                <p id="nome">
                    Animal: 
                </p>

                <form action="{{route('admin.animais.alterar_status')}}" method="POST">

                    <input type="hidden" id="id_animal" name="id">

                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" id="status" name="status" onchange="alterar_motivo_status(this.value);">
                            <option value="0">Em Análise</option>
                            <option value="1">Aprovado</option>
                            <option value="2">Reprovado</option>
                        </select>
                    </div>
                    <div class="form-group" id="motivo_status_div">
                        <label for="motivo_status">Motivo</label>
                        <textarea class="form-control" rows="3" id="motivo_status" name="motivo_status"></textarea>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick="alterar_status();" id="botao_alterar">Salvar</button>
            </div>
        </div>
    </div>
</div>



@endsection



@section('scripts')
<script>
    function alterar_status() {

        id = $('#id_animal').val();
        status = $('#status').val();
        motivo_status = $('#motivo_status').val();

        if ((status == 0 || status == 2) && !motivo_status) {
            alert('Digite o motivo da atualização de status.');
            // $.bootstrapGrowl(', {
            //     type: 'danger',
            //     align: 'right',
            //     width: 'auto',
            //     allow_dismiss: true
            // });

            $('#motivo_status').focus();

            return false;

        }

        $('#botao_alterar').html('<i class="fas fa-circle-notch fa-spin"></i>').attr('disabled', 'disabled');

        $.ajax({
            method: 'POST',
            url: "{{route('admin.animais.alterar_status')}}",
            dataType: 'json',
            data: 'id=' + id + '&status=' + status + '&motivo_status=' + motivo_status,
            success: function(msg) {

                location.reload();

            },
            error: function() {
                alert('Erro ao alterar o status. Por favor, recarregue a página e tente novamente.');
                // $.bootstrapGrowl('Erro ao alterar o status. Por favor, recarregue a página e tente novamente.', {
                //     type: 'success',
                //     align: 'right',
                //     width: 'auto',
                //     allow_dismiss: true
                // })

                $('#botao_alterar').html('Salvar').removeAttr('disabled');

            }
        })

    }




    function abrir_modal_status(id, status) {

        $.ajax({
            method: 'POST',
            url: "{{route('admin.animais.abrir_modal_status')}}",
            dataType: 'json',
            data: 'id=' + id,
            success: function(msg) {
                console.info('sucess');
                console.info(msg);
                $('#id_animal').val(msg.animal.id);

                $('#motivo_status').val(msg.animal.motivo_status);
                $('#nome').html('Animal: ' + msg.animal.nome);

                if (msg.animal.status == 0 || msg.animal.status == 2) {
                    $('#motivo_status_div').hide();
                } else {
                    $('#motivo_status_div').show();
                }
                $("#status").val(msg.animal.status).change();

                $('#modal_status').modal('toggle');

            },
            error: function(msg) {

                console.info('error');
                console.info(msg);
                // $.bootstrapGrowl('Erro ao abrir.', {
                //     type: 'danger',
                //     align: 'right',
                //     width: 'auto',
                //     allow_dismiss: true
                // });
            }
        })

    }

    function alterar_motivo_status(status) {

        if (status == 0 || status == 2) {
            $('#motivo_status_div').fadeIn();
        } else {
            $('#motivo_status_div').fadeOut();
        }

    }
</script>
@endsection