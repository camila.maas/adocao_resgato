@extends('admin.animais.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.animais.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" required>
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3"> </textarea>
                </div>
                <div class="form-group" id="animal_type">
                    <label for="animal_type">Tipo</label>
                    <select class="form-control" name="animal_type" onchange="change_types(this.value)" required>
                        <option value="">Selecione</option>
                        <option value="1">Cachorro</option>
                        <option value="2">Gato</option>
                    </select>
                </div>
                <div class="form-group" id="breed_id">
                    <label for="breed_id">Raça</label>
                    <select class="form-control" name="breed_id" required>
                        <option value="">Selecione o tipo</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="color">Cor</label>
                    <input type="text" class="form-control" name="color" placeholder="Cor">
                </div>
                <div class="form-group">
                    <label for="birth_date">Data de nascimento</label>
                    <input type="text" class="form-control" name="birth_date" placeholder="Data de nascimento">
                </div>
                <div class="form-group" id="size">
                    <label for="size">Porte</label>
                    <select class="form-control" name="size" required>
                        <option value="">Selecione o porte</option>
                        <option value="1">Pequeno</option>
                        <option value="2">Médio</option>
                        <option value="3">Grande</option>
                    </select>
                </div>
                <div class="form-group" id="sex">
                    <label for="sex">Sexo</label>
                    <select class="form-control" name="sex" required>
                        <option value="">Selecione o sexo</option>
                        <option value="1">Macho</option>
                        <option value="2">Fêmea</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="ong">Ong</label>
                    <select class="form-control" name="ong" required>
                        <option value="">Selecione</option>
                        @foreach($ong as $o)
                        <option value="{{$o->id}}">{{$o->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" onchange="change_cities(this.value)" required>
                        <option value="">Selecione</option>
                        @foreach($estados as $estado)
                        <option value="{{$estado->id}}">{{$estado->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group" id="city_id">
                    <label for="city_id">Cidade</label>
                    <select class="form-control" name="city_id">
                        <option value="">Selecione o estado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" name="cep" placeholder="CEP">
                </div>
                <div class="form-group">
                    <label for="street">Rua</label>
                    <input type="text" class="form-control" name="street" placeholder="Rua">
                </div>
                <div class="form-group">
                    <label for="district">Bairro</label>
                    <input type="text" class="form-control" name="district" placeholder="Bairro">
                </div>
                <div class="form-group">
                    <label for="complement">Complemento</label>
                    <input type="text" class="form-control" name="complement" placeholder="Complemento">
                </div>
                <div class="form-group">
                    <label for="number">Número</label>
                    <input type="text" class="form-control" name="number" placeholder="Número">
                </div>
                <div class="form-group" id="ad_type">
                    <label for="ad_type">Tipo do anúncio</label>
                    <select class="form-control" name="ad_type" required>
                        <option value="">Selecione o tipo do anúncio</option>
                        <option value="1">Adoção</option>
                        <option value="2">Doação</option>
                        <option value="3">Resgate</option>
                        <option value="4">Desaparecido</option>
                        <option value="5">Encontrado</option>
                    </select>
                </div>
                <div class="form-group" id="status" hidden>
                        <label for="status">Tipo do anúncio</label>
                        <select class="form-control" name="status" required>
                                <option value="">Selecione o tipo do anúncio</option>
                                <option selected value="0">Em análise</option>
                        </select>
                    </div>
                <div id="images">
                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>
                </div>
                <button type="button" class="btn btn-info btn-xs" onclick="add_images()">Mais fotos</button>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id) {
        $.ajax({
            method: 'POST',
            url: "{{route('admin.cidades.find_by_state_id')}}",
            data: 'id=' + state_id,
            dataType: 'JSON',
            success: function(msg) {
                $('#city_id').html(msg.view);
            },
            error: function(msg) {

            }
        })
    }

    function change_types(type) {
        console.info('entrou');
        $.ajax({
            method: 'POST',
            url: "{{route('admin.racas.find_by_type')}}",
            data: 'type=' + type,
            dataType: 'JSON',
            success: function(msg) {
                $('#breed_id').html(msg.view);
            },
            error: function(msg) {}
        })
    }

    function add_images() {
        $('#images')
            .append(`<div class="form-group">
                        <label>Foto</label>
                        <input type="file" name="images[]">
                        <br>
                        <center>
                            <button type="button" class="btn btn-danger btn-xs" onclick="remove_image(this)">Remover</button>
                        </center>
                    </div>`);
    }

    function remove_image(element) {
        $(element).parent().parent().remove();
    }
</script>
@endsection