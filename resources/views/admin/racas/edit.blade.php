@extends('admin.racas.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.racas.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <input type="hidden" name="id" value="{{$raca->id}}">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{$raca->nome}}">
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3" >{{$raca->descricao}}</textarea>
                </div>
                <div class="form-group" id="type">
                    <label for="type">Tipo</label>
                    <select class="form-control" name="type">
                        <option value="">Selecione o tipo</option>
                        <option @if($raca->tipo == 1) selected @endif value="1">Cachorro</option>
                        <option @if($raca->tipo == 2) selected @endif value="2">Gato</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    function change_cities(state_id){
        $.ajax({
                method: 'POST',
                url: "{{route('admin.cidades.find_by_state_id')}}",
                data:'id='+state_id,
                dataType: 'JSON',
                success: function(msg){
                    $('#city_id').html(msg.view);
                }, error: function(msg){

                }
            })
    }
</script>
@endsection