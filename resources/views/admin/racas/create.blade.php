@extends('admin.racas.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <div class="col-md-7">
            <form action="{{route('admin.racas.save_update')}}" method="POST" class="form" enctype="multipart/form-data">
                {{-- Token que deve ser enviado para o Laravel --}}
                @csrf 
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" placeholder="Nome">
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description" placeholder="Descrição" rows="3"> </textarea>
                </div>
                 <div class="form-group" id="type">
                    <label for="type">Tipo</label>
                    <select class="form-control" name="type">
                        <option value="">Selecione o tipo</option>
                        <option value="1">Cachorro</option>
                        <option value="2">Gato</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-success pull-right">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection