@extends('admin.racas.topo')

@section('modulo')

<div class="col-sm-12">
    <div class="conteudo">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Tipo</th>
                    <th></th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($racas as $r)

                    <tr>
                        <td>{{$r->nome}}</td>
                        <td>{{$r->descricao}}</td>
                        <td>@tipo_raca($r->tipo)</td>
                        <td></td>
                        <td>
                            <a href="{{route('admin.racas.create_edit_view', $r->id)}}" class="btn btn-xs btn-warning">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="{{route('admin.racas.delete', $r->id)}}" class="btn btn-xs btn-danger" data-toggle="confirmation"
                                data-btn-ok-label="Sim" data-btn-ok-class="btn-sm btn-success"
                                data-btn-ok-icon="fa fa-check"
                                data-btn-cancel-label="Não" data-btn-cancel-class="btn-sm btn-danger"
                                data-btn-cancel-icon="fa fa-trash"
                                data-title="Excluir?">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection