<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{

    protected $table = 'eventos';   
    
    public function getEndereco(){
        return $this->belongsTo('App\Endereco', 'id_endereco');
    }

    public function getFotos(){
        return $this->hasMany('App\FotoEvento', 'id_evento');
    }

}
