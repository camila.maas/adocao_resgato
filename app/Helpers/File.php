<?php

namespace App\Helpers;

use Image, Storage;

class File {

    public static function save($file, string $dir) : string {
        
        return Storage::disk('public')->putFile($dir, $file);
    }

    public static function resize(string $image, int $size) : void {

        $image = storage_path('app/public/' . $image);

        Image::make($image)->widen($size)->save($image);
    }
}