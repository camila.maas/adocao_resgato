<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Blade::directive('date', function ($expression) {
            return "<?php echo date('d/m/Y', strtotime($expression));?>";
        });

        Blade::directive('datetime', function ($expression) {
            return "<?php echo date('d/m/Y H:i', strtotime($expression));?>";
        });

        Blade::directive('porte', function ($expression) {
            return "<?php switch($expression){
                case '1':
                    echo 'Pequeno';
                    break;
                case '2':
                    echo 'Médio';
                    break;
                case '3':
                    echo 'Grande';
                    break;
            }?>";
        });

        Blade::directive('sexo_animal', function ($expression) {
            return "<?php switch($expression){
                case '1':
                    echo 'Macho';
                    break;
                case '2':
                    echo 'Fêmea';
                    break;
            }?>";
        });

        Blade::directive('sexo', function ($expression) {
            return "<?php switch($expression){
                case '1':
                    echo 'Masculino';
                    break;
                case '2':
                    echo 'Feminino';
                    break;
            }?>";
        });

        Blade::directive('tipo_raca', function ($expression) {
            return "<?php switch($expression){
                case '1':
                    echo 'Cachorro';
                    break;
                case '2':
                    echo 'Gato';
                    break;
            }?>";
        });

        Blade::directive('nivel_admin', function ($expression) {
            return "<?php switch($expression){
                case '1':
                    echo 'Administrador Sistema';
                    break;
                case '2':
                    echo 'Administrador Ong';
                    break;
            }?>";
        });

        Blade::directive('status', function ($expression) {
            return "<?php switch($expression){
                case '0':
                    echo 'Em Análise';
                    break;
                case '1':
                    echo 'Aprovado';
                    break;
                case '2':
                    echo 'Reprovado';
                    break;
            }?>";
        });
    }
}
