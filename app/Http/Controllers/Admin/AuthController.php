<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;

class AuthController extends Controller
{
    public function login(LoginRequest $request) {
        if (auth()->guard('admin')->attempt([
            'email'    => $request->email,
            'password' => $request->password,
        ], true)) {
            return redirect()->route('admin.dashboard');
        } else {
            return back();
        }
    }

    public function logout() {
        auth()->guard('admin')->logout();

        return redirect()->route('admin.login');
    }
}
