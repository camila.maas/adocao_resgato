<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cidade;

class CidadeController extends Controller
{
    public function index(){
        $cidade = Cidade::all();
        return $cidade;
    }

    public function find_by_state_id(Request $request){
        $cidades = Cidade::where(['id_estado'=>$request->id])->get();
        $view = view('admin.ajax.cidades')->with(['cidades'=> $cidades])->render();
        return response()->json(['view'=>$view]);
    }
}
