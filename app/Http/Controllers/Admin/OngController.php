<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Ong, Estado, Endereco, Cidade, FotoOng, Admin};
use App\Helpers\File;
use Storage;

class OngController extends Controller
{
    
    public function index(){
        $ong = Ong::all();
        return view('admin.ong.index')->with(['ong' => $ong]);
    }

    public function find($id){
        $ong = Ong::find($id);
        return $ong;
    }

    public function create_save_view(){
        $estados = Estado::all();
        $admins = Admin::where(['nivel'=>2])->get();
        return view('admin.ong.create')->with(['estados' => $estados, 'admins'=>$admins]);
    }

    public function create_edit_view($id){
        $estados = Estado::all();
        $admins = Admin::where(['nivel'=>2])->get();
        $ong = Ong::with('getEndereco', 'getFotos', 'getAdmins')->find($id);
        $cidades = Cidade::where(['id_estado'=>$ong->getEndereco->getCidade->id_estado])->get();
        return view('admin.ong.edit')->with(['ong' => $ong, 'estados' => $estados, 'cidades' => $cidades, 'admins'=>$admins]);
    }

    public function save_update(Request $request){
        if($request->address_id){
            $endereco = Endereco::find($request->address_id);
        }else{
            $endereco = new Endereco();
        }        
        $endereco->id_cidade = $request->city_id;
        $endereco->cep = $request->cep;
        $endereco->rua = $request->street;
        $endereco->bairro = $request->district;
        $endereco->complemento = $request->complement;
        $endereco->numero = $request->number;
        $endereco->save();        
        
        if($request->id){
            $ong = Ong::find($request->id);
        }else{
            $ong = new Ong();
        }
        $ong->id_endereco = $endereco->id;
        $ong->nome = $request->name;
        $ong->telefone = $request->phone;
        $ong->email = $request->email;
        $ong->responsavel = $request->responsible;

        if($request->file('file')){
            Storage::disk('public')->delete($ong->logo);
            $logo = File::save($request->file('file'), 'ong/logos');
            File::resize($logo, 500);
            $ong->logo=$logo;
        }

        $ong->save();

        if($request->file('images')){
            foreach ($request->file('images') as $k => $imagem) {
                $image = File::save($imagem, 'ong/imagens');
                File::resize($image, 500);
                $foto = new FotoOng();
                $foto->id_ong = $ong->id;
                $foto->titulo = $imagem->getClientOriginalName();
                $foto->extensao = $imagem->getClientOriginalExtension();
                $foto->caminho = $image;
                $foto->save();
            }
        }

        $ong->getAdmins()->sync($request->admins);

        $message = '';

        if($request->id){
            $message = 'Ong editada com sucesso!';
        }else{
            $message = 'Ong criada com sucesso!';
        }
        
        session()->flash('message', [
            'type' => 'success',
            'message' => $message
        ]);
        
        return redirect()->route('admin.ong.index');
    }

    public function delete_image(Request $request){
        $img = FotoOng::find($request->id);       
        Storage::disk('public')->delete($img->caminho);
        $img->delete();
        return response()->json('', 200);
    }

    public function delete($id){
        $ong = Ong::find($id);
        $endereco_id = $ong->id_endereco;

        Storage::disk('public')->delete($ong->logo);
        
        foreach($ong->getFotos as $foto){
            Storage::disk('public')->delete($foto->caminho);
            $foto->delete();
        }
        
        $ong->getAdmins()->sync([]);

        $ong->delete();

        $endereco = Endereco::find($endereco_id);
        $endereco->delete();
        
        session()->flash('message', [
            'type' => 'success',
            'message' => 'Ong excluída com sucesso!'
        ]);
        
        return redirect()->route('admin.ong.index');
    }
}
