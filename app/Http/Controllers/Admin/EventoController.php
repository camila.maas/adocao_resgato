<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Evento, Estado, Cidade, Endereco, Ong, FotoEvento};
use App\Helpers\File;
use Storage;

class EventoController extends Controller
{

    public function index(){
        $eventos = Evento::all();
        return view('admin.eventos.index')->with(['eventos'=> $eventos]);
    }

    public function find($id){
        $evento = Evento::find($id);
        return $evento;
    }

    public function create_save_view(){
        $estados = Estado::all();
        $ong = Ong::all();
        return view('admin.eventos.create')->with(['estados' => $estados, 'ong'=>$ong]);
    }

    public function create_edit_view($id){
        $estados = Estado::all();
        $ong = Ong::all();
        $evento = Evento::with('getEndereco', 'getFotos')->find($id);
        $cidades = Cidade::where(['id_estado'=>$evento->getEndereco->getCidade->id_estado])->get();
        return view('admin.eventos.edit')->with(['evento' => $evento, 'estados' => $estados, 'ong'=> $ong, 'cidades' => $cidades]);
    }
    
    public function save_update(Request $request){

        $data_ini = explode(' ', $request->start);
        $data_inicial = implode("-", array_reverse(explode("/", trim($data_ini[0])))).' '.$data_ini[1];
        
        $data_fim = explode(' ', $request->end);
        $data_final = implode("-", array_reverse(explode("/", trim($data_fim[0])))).' '.$data_fim[1];

        if($request->address_id){
            $endereco = Endereco::find($request->address_id);
        }else{
            $endereco = new Endereco();
        }        
        $endereco->id_cidade = $request->city_id;
        $endereco->cep = $request->cep;
        $endereco->rua = $request->street;
        $endereco->bairro = $request->district;
        $endereco->complemento = $request->complement;
        $endereco->numero = $request->number;
        $endereco->save();        
     
        if($request->id){
            $evento = Evento::find($request->id);
        }else{
            $evento = new Evento();
        }

        $evento->id_endereco = $endereco->id;
        $evento->id_ong = $request->ong;
        $evento->nome = $request->name;
        $evento->data_hora_inicio = $data_inicial;
        $evento->data_hora_fim = $data_final;
        $evento->local = $request->place;
        $evento->descricao = $request->description;
        $evento->save();
     
        if($request->file('images')){
            foreach ($request->file('images') as $k => $imagem) {
                $image = File::save($imagem, 'eventos/imagens');
                File::resize($image, 500);
                $foto = new FotoEvento();
                $foto->id_evento = $evento->id;
                $foto->titulo = $imagem->getClientOriginalName();
                $foto->extensao = $imagem->getClientOriginalExtension();
                $foto->caminho = $image;
                $foto->save();
            }
        }

        $message = '';

        if($request->id){
            $message = 'Evento editado com sucesso!';
        }else{
            $message = 'Evento criado com sucesso!';
        }
        
        session()->flash('message', [
            'type' => 'success',
            'message' => $message
        ]);
        
        return redirect()->route('admin.eventos.index');

    }

    public function delete_image(Request $request){
        $img = FotoEvento::find($request->id);       
        Storage::disk('public')->delete($img->caminho);
        $img->delete();
        return response()->json('', 200);
    }

    public function delete($id){
        $evento = Evento::find($id);
        $endereco_id = $evento->id_endereco;

        foreach($evento->getFotos as $foto){
            Storage::disk('public')->delete($foto->caminho);
            $foto->delete();
        }

        $evento->delete();

        $endereco = Endereco::find($endereco_id);
        $endereco->delete();

        session()->flash('message', [
            'type' => 'success',
            'message' => 'Evento excluído com sucesso!'
        ]);
        
        return redirect()->route('admin.eventos.index');
    }
}
