<?php

namespace App\Http\Controllers\Admin;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Admin, Ong};

class AdminController extends Controller
{
    public function index(){
        $admins = Admin::all();
        return view('admin.admins.index')->with(['admins' => $admins]);
    }

    public function find($id){
        $admin = Admin::find($id);
        return $admin;
    }

    public function create_save_view(){
        $ong = Ong::all();
        
        if(auth()->guard('admin')->user()->nivel == 2){
            $ong = auth()->guard('admin')->user()->getOng()->get();
        }

        return view('admin.admins.create')->with('ong', $ong);
    }

    public function create_edit_view($id){
        $admin = Admin::with('getOng')->find($id);
        $ong = Ong::all();
        
        if(auth()->guard('admin')->user()->nivel == 2){
            $ong = auth()->guard('admin')->user()->getOng()->get();
        }

        return view('admin.admins.edit')->with(['admin' => $admin, 'ong'=> $ong]);
    }

    public function save_update(Request $request){
        if($request->id){
            $admin = Admin::find($request->id);
        }else{
            $admin = new Admin();
        }

        $admin->nome = $request->name;
        $admin->nivel = $request->level;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->save();
        
        if($admin->nivel == 2){
            $admin->getOng()->sync($request->ong);
        }

        $message = '';
        
        if($request->id){
            $message = 'Administrador editado com sucesso!';
        }else{
            $message = 'Administrador criado com sucesso!';
        }
        
        session()->flash('message', [
            'type' => 'success',
            'message' => $message
        ]);
        
        return redirect()->route('admin.admins.index');
    }

    public function delete($id){
        $admins = Admin::find($id);

        $admins->getOng()->sync([]);

        $admins->forceDelete();

        session()->flash('message', [
            'type' => 'success',
            'message' => 'Administrador excluído com sucesso!'
        ]);
        
        return redirect()->route('admin.admins.index');
    }

}
