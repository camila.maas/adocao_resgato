<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Animal, Raca, Estado, Endereco, Cidade, FotoAnimal, Ong};
use App\Helpers\File;
use Storage;

class AnimalController extends Controller
{
    public function index()
    {
        $animais = Animal::all();
        return view('admin.animais.index')->with(['animais' => $animais]);
    }

    public function find($id)
    {
        $animal = Animal::find($id);
        return $animal;
    }

    public function create_save_view()
    {
        $animais = Animal::all();
        $estados = Estado::all();
        $ong = Ong::all();

        if (auth()->guard('admin')->user()->nivel == 2) {
            $ong = auth()->guard('admin')->user()->getOng()->get();
        }

        return view('admin.animais.create')->with(['animais' => $animais, 'estados' => $estados, 'ong' => $ong]);
    }

    public function create_edit_view($id)
    {
        $estados = Estado::all();
        $racas = Raca::all();
        $animal = Animal::with('getEndereco')->find($id);
        $cidades = Cidade::where(['id_estado' => $animal->getEndereco->getCidade->id_estado])->get();
        $ong = Ong::all();

        if (auth()->guard('admin')->user()->nivel == 2) {
            $ong = auth()->guard('admin')->user()->getOng()->get();
        }

        return view('admin.animais.edit')->with(['animal' => $animal, 'racas' => $racas, 'estados' => $estados, 'cidades' => $cidades, 'ong' => $ong]);
    }

    public function save_update(Request $request)
    {
        if ($request->address_id) {
            $endereco = Endereco::find($request->address_id);
        } else {
            $endereco = new Endereco();
        }
        $endereco->id_cidade = $request->city_id;
        $endereco->cep = $request->cep;
        $endereco->rua = $request->street;
        $endereco->bairro = $request->district;
        $endereco->complemento = $request->complement;
        $endereco->numero = $request->number;
        $endereco->save();

        if ($request->id) {
            $animal = Animal::find($request->id);
        } else {
            $animal = new Animal();
        }

        $animal->id_endereco = $endereco->id;
        $animal->id_raca = $request->breed_id;
        $animal->id_usuario = '1';
        $animal->nome = $request->name;
        $animal->descricao = $request->description;
        $animal->cor = $request->color;
        $animal->data_nascimento = implode("-", array_reverse(explode("/", trim($request->birth_date))));
        $animal->porte = $request->size;
        $animal->sexo = $request->sex;
        $animal->status = $request->status;
        $animal->tipo_tutor = 1;
        $animal->id_ong = $request->ong;
        $animal->tipo_anuncio = $request->ad_type;
        $animal->save();

        if ($request->file('images')) {
            foreach ($request->file('images') as $k => $imagem) {
                $image = File::save($imagem, 'animais/imagens');
                File::resize($image, 500);
                $foto = new FotoAnimal();
                $foto->id_animal = $animal->id;
                $foto->titulo = $imagem->getClientOriginalName();
                $foto->extensao = $imagem->getClientOriginalExtension();
                $foto->caminho = $image;
                $foto->save();
            }
        }

        $message = '';

        if ($request->id) {
            $message = 'Animal editado com sucesso!';
        } else {
            $message = 'Animal criado com sucesso!';
        }

        session()->flash('message', [
            'type' => 'success',
            'message' => $message
        ]);

        return redirect()->route('admin.animais.index');
    }

    public function delete_image(Request $request)
    {
        $img = FotoAnimal::find($request->id);
        Storage::disk('public')->delete($img->caminho);
        $img->delete();
        return response()->json('', 200);
    }

    public function delete($id)
    {
        $animal = Animal::find($id);
        $endereco_id = $animal->id_endereco;

        foreach ($animal->getFotos as $foto) {
            Storage::disk('public')->delete($foto->caminho);
            $foto->delete();
        }

        $animal->delete();

        $endereco = Endereco::find($endereco_id);
        $endereco->delete();

        session()->flash('message', [
            'type' => 'success',
            'message' => 'Animal excluído com sucesso!'
        ]);

        return redirect()->route('admin.animais.index');
    }

    public function alterar_status(Request $request)
    {
        $animal = Animal::find($request->id);
        $animal->status = $request->status;
        if ($animal->status == 1) {
            $request->motivo_status = '';
        }
        $animal->motivo_status = $request->motivo_status;
        $animal->save();

        return response()->json('success', 200);
    }

    public function abrir_modal_status(Request $request)
    {
        $animal = Animal::find($request->id);

        return response()->json(['animal' => $animal]);
    }
}
