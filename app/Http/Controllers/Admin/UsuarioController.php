<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Usuario, Estado, Endereco, Cidade};
use App\Helpers\File;
use Storage;

class UsuarioController extends Controller
{

    public function index(){
        $usuarios = Usuario::all();
        return view('admin.usuarios.index')->with(['usuarios' => $usuarios]);
    }

    public function find($id){
        $usuario = Usuario::find($id);
        return $usuario;
    }

    public function create_save_view(){
        $estados = Estado::all();
        return view('admin.usuarios.create')->with(['estados' => $estados]);
    }

    public function create_edit_view($id){
        $estados = Estado::all();
        $usuario = Usuario::with('getEndereco')->find($id);
        $cidades = Cidade::where(['id_estado'=>$usuario->getEndereco->getCidade->id_estado])->get();
        return view('admin.usuarios.edit')->with(['usuario' => $usuario, 'estados' => $estados, 'cidades' => $cidades]);
    }

    public function save_update(Request $request){
        if($request->address_id){
            $endereco = Endereco::find($request->address_id);
        }else{
            $endereco = new Endereco();
        }        

        $endereco->id_cidade = $request->city_id;
        $endereco->cep = $request->cep;
        $endereco->rua = $request->street;
        $endereco->bairro = $request->district;
        $endereco->complemento = $request->complement;
        $endereco->numero = $request->number;
        $endereco->save();        
        
        if($request->id){
            $usuario = Usuario::find($request->id);
        }else{
            $usuario = new Usuario();
            $usuario->password = bcrypt($request->password);
            $usuario->data_nascimento = implode("-", array_reverse(explode("/", trim($request->birth_date))));
            $usuario->email = $request->email;
            $usuario->sexo = $request->sex;
        }

        $usuario->id_endereco = $endereco->id;
        $usuario->nome = $request->name;
        $usuario->telefone = $request->phone;

        if($request->file('file')){
            Storage::disk('public')->delete($usuario->foto);
            $foto = File::save($request->file('file'), 'usuario/fotos');
            File::resize($foto, 500);
            $usuario->foto=$foto;
        }

        $usuario->save();

        $message = '';

        if($request->id){
            $message = 'Usuário editado com sucesso!';
        }else{
            $message = 'Usuário criado com sucesso!';
        }
        
        session()->flash('message', [
            'type' => 'success',
            'message' => $message
        ]);
        
        return redirect()->route('admin.usuarios.index');

    }

    public function delete($id){
        $usuario = Usuario::find($id);
        $endereco_id = $usuario->id_endereco;
        Storage::disk('public')->delete($usuario->foto);
        $usuario->delete();

        $endereco = Endereco::find($endereco_id);
        $endereco->delete();

        session()->flash('message', [
            'type' => 'success',
            'message' => 'Usuario excluído com sucesso!'
        ]);
        
        return redirect()->route('admin.usuarios.index');
    }

}
