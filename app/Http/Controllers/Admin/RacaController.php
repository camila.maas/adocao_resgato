<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Raca;

class RacaController extends Controller
{

    public function index(){
        $racas = Raca::all();
        return view('admin.racas.index')->with(['racas' => $racas]);
    }

    public function find($id){
        $raca = Raca::find($id);
        return $raca;
    }

    public function create_save_view(){
        return view('admin.racas.create');
    }

    public function create_edit_view($id){
        $raca = Raca::find($id);
        return view('admin.racas.edit')->with(['raca' => $raca]);
    }

    public function find_by_type(Request $request){
        $racas = Raca::where(['tipo'=>$request->type])->get();
        $view = view('admin.ajax.racas')->with(['racas'=> $racas])->render();
        return response()->json(['view'=>$view]);
    }

    public function save_update(Request $request){
        if($request->id){
            $raca = Raca::find($request->id);
        }else{
            $raca = new Raca();
        }

        $raca->tipo = $request->type;
        $raca->nome = $request->name;
        $raca->descricao = $request->description;
        $raca->save();

        $message = '';

        if($request->id){
            $message = 'Raça editada com sucesso!';
        }else{
            $message = 'Raça criada com sucesso!';
        }
        
        session()->flash('message', [
            'type' => 'success',
            'message' => $message
        ]);
        
        return redirect()->route('admin.racas.index');
    }

    public function delete($id){
        $raca = Raca::find($id);
        $raca->delete();
        
        session()->flash('message', [
            'type' => 'success',
            'message' => 'Raça excluída com sucesso!'
        ]);
        
        return redirect()->route('admin.racas.index');

    }

}
