<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Evento;

class EventoController extends Controller
{

    public function index()
    {
        $evento = Evento::all();
        return $evento;
    }

    public function find($id)
    {
        $evento = Evento::with('getFotos', 'getEndereco')->find($id);
        $evento->data_hora_inicio = date('d/m/Y H:i', strtotime($evento->data_hora_inicio));
        $evento->data_hora_fim = date('d/m/Y H:i', strtotime($evento->data_hora_fim));
        $evento->endereco = $evento->getEndereco->getCidade->nome . '-' . $evento->getEndereco->getCidade->uf;

        return $evento;
    }
}
