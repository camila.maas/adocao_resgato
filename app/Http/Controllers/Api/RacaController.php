<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Raca;

class RacaController extends Controller
{

    public function index()
    {
        $raca = Raca::all();
        return $raca;
    }

    public function find($id)
    {
        $raca = Raca::find($id);
        return $raca;
    }

    public function find_by_type($type)
    {
        $racas = Raca::where(['tipo' => $type])->get();
        return response()->json($racas);
    }
}
