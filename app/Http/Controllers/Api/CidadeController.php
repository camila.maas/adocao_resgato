<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cidade;

class CidadeController extends Controller
{

    public function index(){
        $cidade = Cidade::all();
        return $cidade;
    }

    public function find_by_state_id($state_id){
        $cidades = Cidade::where(['id_estado'=>$state_id])->get();
        return response()->json($cidades);
    }

}
