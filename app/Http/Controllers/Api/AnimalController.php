<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Animal, Raca, Estado, Endereco, Cidade, FotoAnimal, Ong};
use App\Helpers\File;
use Storage;

class AnimalController extends Controller
{

    public function index(Request $request)
    {
        $animais = Animal::whereHas('getRaca', function ($query) use ($request) {
            if ($request->tipo) {
                $query->where(['tipo' => $request->tipo]);
            }
        })->get();
        foreach ($animais as $animal) {
            if (count($animal->getFotos) > 0) {
                $animal->foto = env('STORAGE_URL') . $animal->getFotos->first()->caminho;
            } else {
                $animal->foto = '';
            }
        }
        return $animais;
    }

    public function get_all_by_user($user_id)
    {
        $animais = Animal::where(['id_usuario'=>$user_id])->get();
    
        foreach ($animais as $animal) {
            if (count($animal->getFotos) > 0) {
                $animal->foto = env('STORAGE_URL') . $animal->getFotos->first()->caminho;
            } else {
                $animal->foto = '';
            }
        }
        return $animais;
    }

    public function find($id)
    {
        $animal = Animal::with('getEndereco')->find($id);
        if (count($animal->getFotos) > 0) {
            $animal->foto = env('STORAGE_URL') . $animal->getFotos->first()->caminho;
        } else {
            $animal->foto = '';
        }

        $animal->raca = $animal->getRaca->nome;
        $animal->data_nascimento = date('d/m/Y', strtotime($animal->data_nascimento));

        switch ($animal->porte) {
            case '1':
                $animal->porte = 'Pequeno';
                break;
            case '2':
                $animal->porte = 'Médio';
                break;
            case '3':
                $animal->porte = 'Grande';
                break;
        }

        switch ($animal->sexo) {
            case '1':
                $animal->sexo = 'Macho';
                break;
            case '2':
                $animal->sexo = 'Fêmea';
                break;
        }

        if ($animal->tipo_tutor == 1) {
            $animal->tutor = $animal->getOng->nome . ' (Ong)';
        } else {
            $animal->tutor = $animal->getUsuario->nome;
        }

        // $animal->endereco = $animal->getEndereco->getCidade->nome . '-' . $animal->getEndereco->getCidade->uf;
        $animal->estado = $animal->getEndereco->getCidade->id_estado;
        $animal->cidade = $animal->getEndereco->getCidade->id;
        $animal->cep = $animal->getEndereco->cep;
        $animal->rua = $animal->getEndereco->rua;
        $animal->bairro = $animal->getEndereco->bairro;
        $animal->complemento = $animal->getEndereco->complemento;
        $animal->numero = $animal->getEndereco->numero;
        return $animal;
    }

    public function search(Request $request)
    {
        $animais = Animal::whereHas('getRaca', function ($query) use ($request) {
            if ($request->tipo != null) {
                $query->where(['tipo' => $request->tipo]);
            }
        })
            ->where(function ($query) use ($request) {
                if ($request->raca != null) {
                    $query->where(['id_raca' => $request->raca]);
                }
                if ($request->porte != null) {
                    $query->where(['porte' => $request->porte]);
                }
                if ($request->sexo != null) {
                    $query->where(['sexo' => $request->sexo]);
                }
                if ($request->tipo_anuncio != null) {
                    $query->where(['tipo_anuncio' => $request->tipo_anuncio]);
                }
            })
            ->whereHas('getEndereco', function ($query) use ($request) {
                if ($request->cidade != null) {
                    $query->where(['id_cidade' => $request->cidade]);
                }
            })
            ->whereHas('getEndereco.getCidade.getEstado', function ($query) use ($request) {
                if ($request->estado != null) {
                    $query->where(['id' => $request->estado]);
                }
            })
            ->where('status', 1)->get();
        foreach ($animais as $animal) {
            if (count($animal->getFotos) > 0) {
                $animal->foto = env('STORAGE_URL') . $animal->getFotos->first()->caminho;
            } else {
                $animal->foto = '';
            }
        }
        return $animais;
    }

    public function save_update(Request $request)
    {
        if ($request->id_endereco) {
            $endereco = Endereco::find($request->id_endereco);
        } else {
            $endereco = new Endereco();
        }
        $endereco->id_cidade = $request->cidade;
        $endereco->cep = $request->estado;
        $endereco->rua = $request->rua;
        $endereco->bairro = $request->bairro;
        $endereco->complemento = $request->complemento;
        $endereco->numero = $request->numero;
        $endereco->save();

        if ($request->id) {
            $animal = Animal::find($request->id);
        } else {
            $animal = new Animal();
            $animal->cor = $request->cor;
            $animal->data_nascimento = implode("-", array_reverse(explode("/", trim($request->data_nascimento))));
            $animal->porte = $request->porte;
            $animal->sexo = $request->sexo;
            $animal->id_raca = $request->raca;
        }

        $animal->id_endereco = $endereco->id;
        $animal->id_usuario = '1';
        $animal->nome = $request->nome;
        $animal->descricao = $request->descricao;
        $animal->status = 0;
        $animal->tipo_tutor = 2;
        $animal->id_ong = null;
        $animal->tipo_anuncio = $request->tipo_anuncio;
        $animal->save();

        if ($request->file('images')) {
            foreach ($request->file('images') as $k => $imagem) {
                $image = File::save($imagem, 'animais/imagens');
                File::resize($image, 500);
                $foto = new FotoAnimal();
                $foto->id_animal = $animal->id;
                $foto->titulo = $imagem->getClientOriginalName();
                $foto->extensao = $imagem->getClientOriginalExtension();
                $foto->caminho = $image;
                $foto->save();
            }
        }

    }
}
