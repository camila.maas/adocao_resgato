<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Estado;

class EstadoController extends Controller
{

    public function index(){
        $estado = Estado::all();
        return $estado;
    }

}
