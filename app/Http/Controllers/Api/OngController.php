<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ong;

class OngController extends Controller
{

    public function index()
    {
        $ong = Ong::all();
        foreach ($ong as $o) {
            if (count($o->getFotos) > 0) {
                $o->foto = env('STORAGE_URL') . $o->getFotos->first()->caminho;
            } else {
                $o->foto = '';
            }
            if ($o->logo != null) {
                $o->logo = env('STORAGE_URL') . $o->logo;
            }
        }

        return $ong;
    }

    public function find($id)
    {
        $ong = Ong::with('getEventos', 'getEndereco')->find($id);
        if (count($ong->getFotos) > 0) {
            $ong->foto = env('STORAGE_URL') . $ong->getFotos->first()->caminho;
        } else {
            $ong->foto = '';
        }
        if ($ong->logo != null) {
            $ong->logo = env('STORAGE_URL') . $ong->logo;
        }

        if (count($ong->getEventos) > 0) {
            foreach ($ong->getEventos as $evento) {
                $evento->data_hora_inicio = date('d/m/Y H:i', strtotime($evento->data_hora_inicio));
                $evento->data_hora_fim = date('d/m/Y H:i', strtotime($evento->data_hora_fim));
            }
        }
        $ong->endereco = $ong->getEndereco->getCidade->nome .'-'. $ong->getEndereco->getCidade->uf;

        return $ong;
    }
}
