<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\{Usuario, Estado, Endereco, Cidade};
use App\Helpers\File;
use Storage;

class UsuarioController extends Controller
{

    public function find($id)
    {
        $usuario = Usuario::with('getEndereco')->find($id);
        if ($usuario->foto) {
            $usuario->foto =  env('STORAGE_URL') . $usuario->foto;
        }
        $usuario->data_nascimento = date('d/m/Y', strtotime($usuario->data_nascimento));

        $usuario->endereco = $usuario->getEndereco->getCidade->nome . '-' . $usuario->getEndereco->getCidade->uf;
        $usuario->estado = $usuario->getEndereco->getCidade->id_estado;
        $usuario->cidade = $usuario->getEndereco->getCidade->id;
        $usuario->cep = $usuario->getEndereco->cep;
        $usuario->rua = $usuario->getEndereco->rua;
        $usuario->bairro = $usuario->getEndereco->bairro;
        $usuario->complemento = $usuario->getEndereco->complemento;
        $usuario->numero = $usuario->getEndereco->numero;

        return $usuario;
    }

    public function save_update(Request $request)
    {
        if ($request->id_endereco) {
            $endereco = Endereco::find($request->id_endereco);
        } else {
            $endereco = new Endereco();
        }

        $endereco->id_cidade = $request->cidade;
        $endereco->cep = $request->cep;
        $endereco->rua = $request->rua;
        $endereco->bairro = $request->bairro;
        $endereco->complemento = $request->complemento;
        $endereco->numero = $request->numero;
        $endereco->save();

        if ($request->id) {
            $usuario = Usuario::find($request->id);
        } else {
            $usuario = new Usuario();
            $usuario->password = $request->password;
            $usuario->email = $request->email;
            $usuario->data_nascimento = implode("-", array_reverse(explode("/", trim($request->birth_date))));
            $usuario->sexo = $request->sex;
        }

        $usuario->id_endereco = $endereco->id;
        $usuario->nome = $request->nome;
        $usuario->telefone = $request->telefone;

        if ($request->file('file')) {
            Storage::disk('public')->delete($usuario->foto);
            $foto = File::save($request->file('file'), 'usuario/fotos');
            File::resize($foto, 500);
            $usuario->foto = $foto;
        }

        $usuario->save();

        return $usuario;
    }
}
