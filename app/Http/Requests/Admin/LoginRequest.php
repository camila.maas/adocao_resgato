<?php

namespace App\Http\Requests\Admin;

use App\Admin;
use GuzzleHttp\Client;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class LoginRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email'                => 'required|email',
            'password'             => 'required',
            'g-recaptcha-response' => 'required',
        ];
    }

    public function messages() {
        return [
            'email.required'                => 'Insira o email!',
            'email.email'                   => 'Insira um email válido!',
            'password.required'             => 'Insira a senha!',
            'g-recaptcha-response.required' => 'Selecione não sou robô!',
        ];
    }

    //função personalizada de validação
    public function withValidator($validator) {
        $validator->after(function ($validator) {
            //validação de usuario
            $usuario = Admin::where([
                'email' => $validator->getData()['email'],
            ])->first();

            if (blank($usuario)) {
                $validator->errors()->add('email', 'Esse email não existe!');
            } elseif (!Hash::check($validator->getData()['password'], $usuario['password'])) {
                $validator->errors()->add('senha', 'Senha inválida!');
            }

            $url = 'https://www.google.com/recaptcha/api/siteverify';

            $client = new Client();

            $response = $client->post($url, [
                'headers'     => [
                    'Cache-Control' => 'no-cache',
                    'Content-Type'  => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                    'secret'   => env('RECAPTCHA_SECRET_KEY'),
                    'response' => $validator->getData()['g-recaptcha-response'],
                ],
            ]);

            $response = json_decode($response->getBody()->getContents());

            //a chamada retorna sempre com success = true ou false
            if (false == $response->success) {
                //retorna a mensagem de erro
                $validator->errors()->add('g-recaptcha-response', 'Código reCAPTCHA inválido!');
            }
        });
    }
}
