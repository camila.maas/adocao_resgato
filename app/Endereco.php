<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    
    protected $table = 'enderecos';
    
    public function getCidade(){
        return $this->belongsTo('App\Cidade', 'id_cidade');
    }


}
