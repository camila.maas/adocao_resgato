<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ong extends Model
{
    
    protected $table = 'ong';

    public function getEndereco(){
        return $this->belongsTo('App\Endereco', 'id_endereco');
    }

    public function getFotos(){
        return $this->hasMany('App\FotoOng', 'id_ong');
    }

    public function getAdmins() {
        return $this->belongsToMany('App\Admin', 'admins_ong', 'id_ong', 'id_admin');
    }

    public function getEventos() {
        return $this->hasMany('App\Evento', 'id_ong');
    }
}
