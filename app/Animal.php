<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{

    protected $table = 'animais';

    public function getEndereco(){
        return $this->belongsTo('App\Endereco', 'id_endereco');
    }

    public function getFotos(){
        return $this->hasMany('App\FotoAnimal', 'id_animal');
    }

    public function getRaca(){
        return $this->belongsTo('App\Raca', 'id_raca');
    }

    public function getOng(){
        return $this->belongsTo('App\Ong', 'id_ong');
    }

    public function getUsuario(){
        return $this->belongsTo('App\Usuario', 'id_usuario');
    }
}
