<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{

    protected $table = 'cidades';
   
    public function getEstado(){
        return $this->belongsTo('App\Estado', 'id_estado');
    }

}
